package com.gotprint.notes.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.gotprint.notes.dao.HibernetDAO;
import com.gotprint.notes.model.Note;
import com.gotprint.notes.model.User;
import com.gotprint.notes.util.ShareUtil;

@Path("/notes")
public class NotesService {
	@POST
	@Path("/create")
	@Produces("application/json")
	@Consumes("application/json")

	public Response createNotes(Note note) {
		System.out.println(" The Note  title" + note.getTitle());
		String output = "Hello note " + note.getTitle();
		return Response.status(200).entity(output).build();

	}

	@GET
	@Path("/search")
	@Produces("application/json")
	public Response RetrieveNotes() {
		String output = "Hello note ";
		Note note = new Note();
		note.setTitle("Yogesh jarad");
		note.setTitle("Note application demo");
		User user = new User();
		try {
			HibernetDAO.insertNote(user);
		} catch (Exception e) {
			System.out.println("Exception ");
			e.printStackTrace();
		}
		return Response.status(200).entity(ShareUtil.genrateJson(note)).build();

	}

	@PUT
	@Path("/update")
	@Produces("application/json")
	@Consumes("application/json")
	public Response updateNotes(Note note) {
		String output = "Hello note ";

		return Response.status(200).entity(output).build();

	}

	@DELETE
	@Path("/delete")
	@Produces("application/json")
	@Consumes("application/json")
	public Response deleteNotes(Note note) {
		String output = "Hello note ";

		return Response.status(200).entity(output).build();

	}

}
