package com.gotprint.notes.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.gotprint.notes.model.Note;
import com.gotprint.notes.model.User;

public class HibernateUtil {
	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			// load from different directory
			SessionFactory sessionFactory = new Configuration().addAnnotatedClass(User.class)
					.addAnnotatedClass(Note.class).configure("hibernate.cfg.xml").buildSessionFactory();
			System.out.println("Hibernate Seesion factory" + sessionFactory);
			return sessionFactory;

		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}

}
