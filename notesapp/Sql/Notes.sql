CREATE DATABASE `newsdb`;
use newsdb;
CREATE TABLE `user` (
 `email` varchar(45) NOT NULL ,
 `password` varchar(45) ,
  'create_time' TIMESTAMP, 
  'last_update_time' TIMESTAMP, 
 CONSTRAINT email_unique UNIQUE (email),
 PRIMARY KEY (`email`)
);
CREATE TABLE `notes` (
 `title` varchar(70) NOT NULL,
 `note` varchar(250) ,
 `create_time` TIMESTAMP,
 `last_update_time` TIMESTAMP,
 PRIMARY KEY (`title`)
);
 
CREATE TABLE `category_article` (
 `user_id`  varchar(70)  NOT NULL,
 `note_id`  varchar(70)  NOT NULL,
 PRIMARY KEY (`user_id`,`note_id`),
 UNIQUE KEY `email_UNIQUE` (`user_id`),
 KEY `fk_user` (`user_id`),
 KEY `fk_title_note` (`note_id`),
 CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`email`),
 CONSTRAINT `fk_notes` FOREIGN KEY (`note`) REFERENCES `notes` (`note`)
);


CREATE TABLE `notes` (
  `title` varchar(70) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`title`),
  KEY `email_id` (`email_id`),
  CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`email_id`) REFERENCES `users` (`email`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `email` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

